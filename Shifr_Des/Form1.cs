﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;


namespace Shifr_Des
{
    public partial class Form1 : Form
    {
		private const int sizeOfSymbol = 16;// Кол-во битов на один символ
		private const int sizeOfBlockByte = 128; //в DES размер блока 64 бит, но поскольку в unicode символ в два раза длинее, то увеличим блок тоже в два раза
		private int sizeOfBlock;//
		public string S { get; set; }// Вводимая строка
		public int Controlk { get; set; }
		public String Key { get; set; }//Ключ к шифрованию
		public String[] Blocks { get; set; }//Ключ к шифрованию
		int[] ip; //массив прямой битовой перестановки
		int[] ip_reverse; //массив обратной битовой перестановки
		int[] expansion; //массив для функции расширения
		int[] key_form; //массив для формирования блоков C и D
		int[] shiftkey; //массив для окончательного формирования ключа длиной 48 бит
		int[] p_set; //перестановка блока
		int[] cycle_move; //для циклической перестановки
		byte[,,] s_block; //матрица для кодирования блоков S сообщения
		public String Filetext; //текст
		public Form1()
        {
            InitializeComponent();
		
	
			sizeOfBlock = sizeOfBlockByte / sizeOfSymbol;
			richTextBox1.Text = Filetext;
			Open_file.Filter = "Text  files(*.txt)|*.txt|All files(*.*)|*.*";
			Save_file.Filter = "Text  files(*.txt)|*.txt|All files(*.*)|*.*";
			ip = new int[64] { 58, 50, 42, 34, 26, 18, 10, 2,
							   60, 52, 44, 36, 28, 20, 12, 4,
							   62, 54, 46, 38, 30, 22, 14, 6,
							   64, 56, 48, 40, 32, 24, 16, 8,
							   57, 49, 41, 33, 25, 17,  9, 1,
							   59, 51, 43, 35, 27, 19, 11, 3,
							   61, 53, 45, 37, 29, 21, 13, 5,
							   63, 55, 47, 39, 31, 23, 15, 7 };
			ip_reverse = new int[64] {  40, 8, 48, 16, 56, 24, 64, 32,
										39, 7, 47, 15, 55, 23, 63, 31,
										38, 6, 46, 14, 54, 22, 62, 30,
										37, 5, 45, 13, 53, 21, 61, 29,
										36, 4, 44, 12, 52, 20, 60, 28,
										35, 3, 43, 11, 51, 19, 59, 27,
										34, 2, 42, 10, 50, 18, 58, 26,
										33, 1, 41,  9, 49, 17, 57, 25 };
			expansion = new int[48] { 32, 1, 2, 3, 4, 5, 4, 5,
									   6, 7, 8, 9, 8, 9, 10, 11,
									  12, 13, 12, 13, 14, 15, 16, 17,
									  16, 17, 18, 19, 20, 21, 20, 21,
									  22, 23, 24, 25, 24, 25, 26, 27,
									  28, 29, 28, 29, 30, 31, 32, 1 };
			key_form = new int[56] { 57, 49, 41, 33, 25, 17, 9,
									  1, 58, 50, 42, 34, 26, 18,
									 10,  2, 59, 51, 43, 35, 27,
									 19, 11,  3, 60, 52, 44, 36,
									 63, 55, 47, 39, 31, 23, 15,
									  7, 62, 54, 46, 38, 30, 22,
									 14,  6, 61, 53, 45, 37, 29,
									 21, 13,  5, 28, 20, 12,  4 };
			s_block = new byte[8, 4, 16]{
							   {{0x0e, 0x04, 0x0d, 0x01, 0x02, 0x0f, 0x0b, 0x08, 0x03, 0x0a, 0x06, 0x0c, 0x05, 0x09, 0x00, 0x07},
								{0x00, 0x0f, 0x07, 0x04, 0x0e, 0x02, 0x0d, 0x01, 0x0a, 0x06, 0x0c, 0x0b, 0x09, 0x05, 0x03, 0x08},
								{0x04, 0x01, 0x04, 0x08, 0x0d, 0x06, 0x02, 0x0b, 0x0f, 0x0c, 0x09, 0x07, 0x03, 0x0a, 0x05, 0x00},
								{0x0f, 0x0c, 0x08, 0x02, 0x04, 0x09, 0x01, 0x07, 0x05, 0x0b, 0x03, 0x0e, 0x0a, 0x00, 0x06, 0x0d}},
							   {{0x0f, 0x01, 0x08, 0x0e, 0x06, 0x0b, 0x03, 0x04, 0x09, 0x07, 0x02, 0x0d, 0x0c, 0x00, 0x05, 0x0a},
								{0x03, 0x0d, 0x04, 0x07, 0x0f, 0x02, 0x08, 0x0e, 0x0c, 0x00, 0x01, 0x0a, 0x06, 0x09, 0x0b, 0x05},
								{0x00, 0x0e, 0x07, 0x0b, 0x0a, 0x04, 0x0d, 0x01, 0x05, 0x08, 0x0c, 0x06, 0x09, 0x03, 0x02, 0x0f},
								{0x0d, 0x08, 0x0a, 0x01, 0x03, 0x0f, 0x04, 0x02, 0x0b, 0x06, 0x07, 0x0c, 0x00, 0x05, 0x0e, 0x09}},
							   {{0x0a, 0x00, 0x09, 0x0e, 0x06, 0x03, 0x0f, 0x05, 0x01, 0x0d, 0x0c, 0x07, 0x0b, 0x04, 0x02, 0x08},
								{0x0d, 0x07, 0x00, 0x09, 0x03, 0x04, 0x06, 0x0a, 0x02, 0x08, 0x05, 0x0e, 0x0c, 0x0b, 0x0f, 0x01},
								{0x0d, 0x06, 0x04, 0x09, 0x08, 0x0f, 0x03, 0x00, 0x0b, 0x01, 0x02, 0x0c, 0x05, 0x0a, 0x0e, 0x07},
								{0x01, 0x0a, 0x0d, 0x00, 0x06, 0x09, 0x08, 0x07, 0x04, 0x0f, 0x0e, 0x03, 0x0b, 0x05, 0x02, 0x0c}},
							   {{0x07, 0x0d, 0x0e, 0x03, 0x00, 0x06, 0x09, 0x0a, 0x01, 0x02, 0x08, 0x05, 0x0b, 0x0c, 0x04, 0x0f},
								{0x0d, 0x08, 0x0b, 0x05, 0x06, 0x0f, 0x00, 0x03, 0x04, 0x07, 0x02, 0x0c, 0x01, 0x0a, 0x0e, 0x09},
								{0x0a, 0x06, 0x09, 0x00, 0x0c, 0x0b, 0x07, 0x0d, 0x0f, 0x01, 0x03, 0x0e, 0x05, 0x02, 0x08, 0x04},
								{0x03, 0x0f, 0x00, 0x06, 0x0a, 0x01, 0x0d, 0x08, 0x09, 0x04, 0x05, 0x0b, 0x0c, 0x07, 0x02, 0x0e}},
							   {{0x02, 0x0c, 0x04, 0x01, 0x07, 0x0a, 0x0b, 0x06, 0x08, 0x05, 0x03, 0x0f, 0x0d, 0x00, 0x0e, 0x09},
								{0x0e, 0x0b, 0x02, 0x0c, 0x04, 0x07, 0x0d, 0x01, 0x05, 0x00, 0x0f, 0x0a, 0x03, 0x09, 0x08, 0x06},
								{0x04, 0x02, 0x01, 0x0b, 0x0a, 0x0d, 0x07, 0x08, 0x0f, 0x09, 0x0c, 0x05, 0x06, 0x03, 0x00, 0x0e},
								{0x0b, 0x08, 0x0c, 0x07, 0x01, 0x0e, 0x02, 0x0d, 0x06, 0x0f, 0x00, 0x09, 0x0a, 0x04, 0x05, 0x03}},
							   {{0x0c, 0x01, 0x0a, 0x0f, 0x09, 0x02, 0x06, 0x08, 0x00, 0x0d, 0x03, 0x04, 0x0e, 0x07, 0x05, 0x0b},
								{0x0a, 0x0f, 0x04, 0x02, 0x07, 0x0c, 0x09, 0x05, 0x06, 0x01, 0x0d, 0x0e, 0x00, 0x0b, 0x03, 0x08},
								{0x09, 0x0e, 0x0f, 0x05, 0x02, 0x08, 0x0c, 0x03, 0x07, 0x00, 0x04, 0x0a, 0x01, 0x0d, 0x01, 0x06},
								{0x04, 0x03, 0x02, 0x0c, 0x09, 0x05, 0x0f, 0x0a, 0x0b, 0x0e, 0x01, 0x07, 0x06, 0x00, 0x08, 0x0d}},
							   {{0x04, 0x0b, 0x02, 0x0e, 0x0f, 0x00, 0x08, 0x0d, 0x03, 0x0c, 0x09, 0x07, 0x05, 0x0a, 0x06, 0x01},
								{0x0d, 0x00, 0x0b, 0x07, 0x04, 0x09, 0x01, 0x0a, 0x0e, 0x03, 0x05, 0x0c, 0x02, 0x0f, 0x08, 0x06},
								{0x01, 0x04, 0x0b, 0x0d, 0x0c, 0x03, 0x07, 0x0e, 0x0a, 0x0f, 0x06, 0x08, 0x00, 0x05, 0x09, 0x02},
								{0x06, 0x0b, 0x0d, 0x08, 0x01, 0x04, 0x0a, 0x07, 0x09, 0x05, 0x00, 0x0f, 0x0e, 0x02, 0x03, 0x0c}},
							   {{0x0d, 0x02, 0x08, 0x04, 0x06, 0x0f, 0x0b, 0x01, 0x0a, 0x09, 0x03, 0x0e, 0x05, 0x00, 0x0c, 0x07},
								{0x01, 0x0f, 0x0d, 0x08, 0x0a, 0x03, 0x07, 0x04, 0x0c, 0x05, 0x06, 0x0b, 0x00, 0x0e, 0x09, 0x02},
								{0x07, 0x0b, 0x04, 0x01, 0x09, 0x0c, 0x0e, 0x02, 0x00, 0x06, 0x0a, 0x0d, 0x0f, 0x03, 0x05, 0x08},
								{0x02, 0x01, 0x0e, 0x07, 0x04, 0x0a, 0x08, 0x0d, 0x0f, 0x0c, 0x09, 0x00, 0x03, 0x05, 0x06, 0x0b}}};
			p_set = new int[32] {   16,  7, 20, 21, 29, 12, 28, 17,
										 1, 15, 23, 26,  5, 18, 31, 10,
										 2,  8, 24, 14, 32, 27,  3,  9,
										 19, 13, 30,  6, 22, 11,  4, 25};
			shiftkey = new int[48] { 14, 17, 11, 24, 1, 5,
									  3, 28, 15, 6, 21, 10,
									 23, 19, 12, 4, 26, 8,
									 16, 7, 27, 20, 13, 2,
									 41, 52, 31, 37, 47, 55,
									 30, 40, 51, 45, 33, 48,
									 44, 49, 39,56, 34, 53,
									 46, 42, 50, 36, 29, 32};
			cycle_move = new int[16] { 1, 1, 2, 2,
									   2, 2, 2, 2,
									   1, 2, 2, 2,
										2, 2, 2, 1 };
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Controlk = 0;
			richTextBox1.Text = S;
			richTextBox5.Text = Key;
		}
		private string StringToNormalLength(string input)
		{
			while (input.Length % sizeOfBlock != 0)
			{
				input += "#";
			}
			return input;
		}
		private void StringToBlocks(string input)
		{
			//Метод разбивающий строку на блоки и переводящий её в двоичный формат
			Blocks = new string[input.Length / sizeOfBlock];

			for (int i = 0; i < Blocks.Length; i++)
			{

				Blocks[i] = input.Substring(i * sizeOfBlock, sizeOfBlock);
				Blocks[i] = StringToByteFormat(Blocks[i]);
			}
		}
		private void ByteToBlocks(string input)
		{
			//Метод разбивающий строку на блоки в двоичном формате
			Blocks = new string[input.Length / sizeOfBlockByte];

			for (int i = 0; i < Blocks.Length; i++)
			{
				Blocks[i] = input.Substring(i * sizeOfBlockByte, sizeOfBlockByte);
			}
		}
		private string FullKey(string key)
		{
			//Доводит ключ до полной длины
			while (key.Length < (sizeOfBlock))
			{
				key = "A" + key;
			}
			if (key.Length > (sizeOfBlock))
				key = key.Substring(0, (sizeOfBlock));
			return key;
		}
		private string StringToByteFormat(string input)
		{
			//Метод, переводящий строку в двоичный формат
			string output = "";
			for (int i = 0; i < input.Length; i++)
			{
				string char_binary = Convert.ToString(input[i], 2);

				while (char_binary.Length < sizeOfSymbol)
					char_binary = "0" + char_binary;
				output += char_binary;
			}
			return output;
		}
		private string DES_Encode_Oneround(string input, string key)
		{
			string L = input.Substring(0, input.Length / 2);
			string R = input.Substring(input.Length / 2, input.Length / 2);
			return (R + XOR(L, f(R, key)));
		}
		private string DES_Decode_Oneround(string input, string key)
		{
			string L = input.Substring(0, input.Length / 2);
			string R = input.Substring(input.Length / 2, input.Length / 2);
			return (XOR(f(L, key), R) + L);
		}
		private string BitToStringFormat(string input)
		{
			//Метод, переводящий строку в символьный формат
			string output = "";
			while (input.Length > 0)
			{
				string char_binary = input.Substring(0, sizeOfSymbol);
				input = input.Remove(0, sizeOfSymbol);

				int a = 0;
				int degree = char_binary.Length - 1;

				foreach (char c in char_binary)
					a += Convert.ToInt32(c.ToString()) * (int)Math.Pow(2, degree--);
				output += ((char)a).ToString();
			}

			return output;
		}
		private string f(string s1, string s2)
		{
			return XOR(s1, s2);
		}
		private string XOR(string s1, string s2)
		{

			//Метод для операции XOR
			string result = "";
			string kek = s2;
			while (s1.Length > s2.Length)
			{
				s2 = s2 + kek;
			}
			for (int i = 0; i < s1.Length; i++)
			{
				bool a = Convert.ToBoolean(Convert.ToInt32(s1[i].ToString()));
				bool b = Convert.ToBoolean(Convert.ToInt32(s2[i].ToString()));

				if (a ^ b)
					result += "1";
				else
					result += "0";
			}
			return result;
		}

		/*private void Encode()
		{
			Key = richTextBox5.Text;
			richTextBox3.Text = StringToByteFormat(S);
			Key = FullKey(Key);
			S = StringToNormalLength(S);
			StringToBlocks(S);
			Key = StringToByteFormat(Key);
			S = "";
			for (int i = 0; i < Blocks.Length; i++)
			{



				Blocks[i] = DES_Encode_Oneround(Blocks[i], Key);
				S += BitToStringFormat(Blocks[i]);
			}
			richTextBox2.Text = S;
		}*/
		private void Output()
		// Вывод в файл
		{
			StreamWriter sw = new StreamWriter("out1.txt");
			sw.WriteLine(S);
			sw.Close();
			Process.Start("out1.txt");
		}

		

		private void button2_Click(object sender, EventArgs e)
		{
			if (radioButton1.Checked)
			{
				richTextBox5.Text += "";
				S = richTextBox1.Text;
				Key = richTextBox5.Text;
				richTextBox3.Text = StringToByteFormat(S);
				Key = FullKey(Key);
				S = StringToNormalLength(S);
				StringToBlocks(S);
				Key = StringToByteFormat(Key);
				S = "";
				for (int i = 0; i < Blocks.Length; i++)
				{



					Blocks[i] = DES_Encode_Oneround(Blocks[i], Key);
					S += BitToStringFormat(Blocks[i]);
				}
				richTextBox2.Text = S;
				//Encode();
				Output();
			}
			if(radioButton2.Checked)
			{
				
				OpenFileDialog read = new OpenFileDialog();
				read.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
				if (read.ShowDialog() == DialogResult.OK)
				{
					richTextBox4.Text = File.ReadAllText(read.FileName, Encoding.Default);
				}
				S = richTextBox2.Text;
				StringToBlocks(S);
				S = "";
				for (int i = 0; i < Blocks.Length; i++)
				{
					Blocks[i] = DES_Decode_Oneround(Blocks[i], Key);
					S += BitToStringFormat(Blocks[i]);
				}
				for (int i = 0; i < S.Length; i++)
				{
					if (S[i] == '#')
					{
						S = S.Remove(i);
					}
				}

				richTextBox4.Text = S;
				StreamWriter sw = new StreamWriter("out2.txt");
				sw.WriteLine(S);
				sw.Close();
				Process.Start("out2.txt");
			}

		}

		private void button3_Click(object sender, EventArgs e)
		{
			if (Open_file.ShowDialog() == DialogResult.Cancel)
				return;
			string filename = Open_file.FileName;
			Filetext = System.IO.File.ReadAllText(filename);
			richTextBox1.Text = Filetext;
		}

		private void Key_sh_Click(object sender, EventArgs e)
		{
			string key_generate = "abcdefghijklmnopqrstuvwxyz0123456789";
			int key_length = 4;
			richTextBox5.Text = Generate_Random_string(key_generate, key_length);
		}
		private string Generate_Random_string(string in_string, int lengt)//генерация ключа из строки
		{
			Random rnd = new Random();
			StringBuilder gen_random_key = new StringBuilder(lengt);
			int key_pos = 0;
			for (int i = 0; i < lengt; i++)
			{
				key_pos = rnd.Next(0, in_string.Length - 1);
				gen_random_key.Append(in_string[key_pos]);
			}
			return gen_random_key.ToString();
		}

		private void radioButton1_CheckedChanged(object sender, EventArgs e)
		{
			if (radioButton1.Checked == true)
			{
				button2.Enabled = true;
			}
		}

		private void radioButton2_CheckedChanged(object sender, EventArgs e)
		{
			if (radioButton2.Checked == true)
			{
				button3.Enabled = true;
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if (Save_file.ShowDialog() == DialogResult.Cancel)
				return;
			string filename = Save_file.FileName;
			System.IO.File.WriteAllText(filename, richTextBox2.Text);
		}
	}
}
